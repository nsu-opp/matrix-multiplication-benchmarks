#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <stdbool.h>


float * mat_alloc_aligned(size_t size) {
    const size_t alignment = sizeof(float) * 4;
    const size_t size_bytes = size * size * sizeof(float);
    return (float *) aligned_alloc(alignment, size_bytes);
}


void mat_free_aligned(float ** mat) {
    free(*mat);
    *mat = NULL;
}


float * mat_set_zero(
        float * mat,
        size_t size) {
    const size_t size_bytes = size * size * sizeof(float);
    return memset(mat, 0, size_bytes);
}


float * mat_mul_i_j_k(
        const float * a,
        const float * b,
        float * result,
        size_t size
) {
    if (NULL == a || NULL == b || NULL == result) {
        fprintf(stderr, "Invalid input");
        return NULL;
    }

    for (size_t i = 0; i < size; i += 1) {
        for (size_t j = 0; j < size; j += 1) {
            result[i * size + j] = 0.0f;

            for (size_t k = 0; k < size; k += 1) {
                result[i * size + j] += a[i * size + k] * b[k * size + j];
            }
        }
    }

    return result;
}


float * mat_mul_i_k_j(
        const float * a,
        const float * b,
        float * result,
        size_t size
) {
    if (NULL == a || NULL == b || NULL == result) {
        fprintf(stderr, "Invalid input");
        return NULL;
    }

    result = mat_set_zero(result, size);

    for (size_t i = 0; i < size; i += 1) {
        for (size_t k = 0; k < size; k += 1) {
            for (size_t j = 0; j < size; j += 1) {
                result[i * size + j] += a[i * size + k] * b[k * size + j];
            }
        }
    }

    return result;
}


void mat_print(
        float * mat,
        size_t size) {
    if (NULL == mat) {
        fprintf(stderr, "Invalid input");
    }

    for (size_t i = 0; i < size * size; i += 1) {
        printf("%+.2e\t", mat[i]);

        if (0 == (i + 1) % size) {
            printf("\n");
        }
    }
}


bool mat_same(
        const float * a,
        const float * b,
        size_t size) {
    const size_t size_bytes = size * size * sizeof(float);
    return !memcmp(a, b, size_bytes);
}


float * mat_fill(
        float * mat,
        size_t size) {
    if (NULL == mat) {
        fprintf(stderr, "Invalid input");
        return NULL;
    }

    for (size_t i = 0; i < size * size; i += 1) {
        mat[i] = (float) (i + 1);
    }

    return mat;
}


inline double now() {
    const double clocks = clock();
    return clocks / CLOCKS_PER_SEC;
}


inline double mind(
        double a,
        double b) {
    return a < b ? a : b;
}


int main() {
    const size_t n_tests = 10;
    const size_t size = 768;

    float * a = mat_alloc_aligned(size);
    float * b = mat_alloc_aligned(size);
    float * result_i_j_k = mat_alloc_aligned(size);
    float * result_i_k_j = mat_alloc_aligned(size);

    if (NULL == a || NULL == b || NULL == result_i_j_k || NULL == result_i_k_j) {
        perror("Failed to allocate memory");
        return EXIT_FAILURE;
    }

    a = mat_fill(a, size);
    b = mat_fill(b, size);

    double time = (double) UINT64_MAX;

    printf("Testing i j k\n");
    fflush(stdout);
    for (size_t i = 0; i < n_tests; i += 1) {
        const double start = now();
        result_i_j_k = mat_mul_i_j_k(a, b, result_i_j_k, size);
        const double end = now();

        printf("[Test #%ld] Time: %.2lfs\n", i, end - start);
        fflush(stdout);
        time = mind(time, end - start);
    }
    printf("Time: %.2lfs\n", time);
    fflush(stdout);

    printf("Testing i k j\n");
    fflush(stdout);
    for (size_t i = 0; i < n_tests; i += 1) {
        const double start = now();
        result_i_k_j = mat_mul_i_k_j(a, b, result_i_k_j, size);
        const double end = now();

        printf("[Test #%ld] Time: %.2lfs\n", i, end - start);
        fflush(stdout);
        time = mind(time, end - start);
    }
    printf("Time: %.2lfs\n", time);
    fflush(stdout);

    printf("Same result? %d\n", mat_same(result_i_j_k, result_i_k_j, size));

    mat_free_aligned(&a);
    mat_free_aligned(&b);
    mat_free_aligned(&result_i_j_k);
    mat_free_aligned(&result_i_k_j);

    return EXIT_SUCCESS;
}
